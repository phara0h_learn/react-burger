import * as actionTypes from './actionTypes';
import axios from 'axios';

const authSuccess = authData => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: authData.idToken,
        userId: authData.localId,
        refreshToken: authData.refreshToken
    };
};

const authFail = error => {
    return {
        type: actionTypes.AUTH_FAIL,
        error
    };
};

const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('experationData');
    localStorage.removeItem('userId');
    localStorage.removeItem('refreshToken');
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

const logoutAfter = expirationTime => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000);
    };
};

export const auth = (email, password, method) => {
    return dispatch => {
        dispatch(authStart());
        const URL = {
            signup: 'signupNewUser',
            login: 'verifyPassword'
        };

        axios
            .post(
                'https://www.googleapis.com/identitytoolkit/v3/relyingparty/' +
                    URL[method] +
                    '?key=AIzaSyAi25GS5ekqYenBSTEyTptSrqJPX_Q9Sy0',
                { email, password, returnSecureToken: true }
            )
            .then(response => {
                const experationData = new Date(new Date().getTime() + response.data.expiresIn * 1000);
                localStorage.setItem('token', response.data.idToken)
                localStorage.setItem('experationData', experationData)
                localStorage.setItem('userId', response.data.localId)
                localStorage.setItem('refreshToken', response.data.refreshToken)
                dispatch(authSuccess(response.data));
                dispatch(logoutAfter(response.data.expiresIn));
            })
            .catch(err => {
                dispatch(authFail(err.response.data.error));
            });
    };
};

export const setAuthRedirectPath = path => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path
    };
};

export const authCheckLocalStore = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch( logout() );
        } else {
            const experationData = new Date(localStorage.getItem('experationData'));
            if ( experationData > new Date() ) {
                dispatch( authSuccess({
                    idToken: token,
                    localId: localStorage.getItem('userId'),
                    refreshToken: localStorage.getItem('refreshToken')
                }));
                const timeDiff = (experationData.getTime() - new Date().getTime()) / 1000;
                dispatch(logoutAfter(timeDiff))
            } else {
                dispatch( logout() );
            }

        }
    }
}