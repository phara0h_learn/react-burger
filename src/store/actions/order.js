import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';

const purchaseBurgerSuccess = (id, orderData) => {
    return {
        type: actionTypes.PURCHASE_BURGER_SUCCESS,
        orderId: id,
        orderData
    };
};

const purchaseBurgerFailed = error => {
    return {
        type: actionTypes.PURCHASE_BURGER_FAIL,
        error
    };
};

export const initPurchaseBurger = () => {
    return {
        type: actionTypes.PURCHASE_BURGER_INIT
    };
};

const startPurchaseBurger = () => {
    return {
        type: actionTypes.PURCHASE_BURGER_START
    };
};

export const purchaseBurger = (orderData, token) => {
    return dispatch => {
        dispatch(startPurchaseBurger());
        axios
            .post('/orders.json?auth=' + token, orderData)
            .then(response => {
                dispatch(purchaseBurgerSuccess(response.data.name, orderData));
            })
            .catch(error => {
                dispatch(purchaseBurgerFailed(error));
            });
    };
};

const fetchOrderSuccess = orders => {
    return {
        type: actionTypes.FETCH_ORDER_SUCCESS,
        orders
    };
};

const fetchOrderFail = error => {
    return {
        type: actionTypes.FETCH_ORDER_FAIL,
        error
    };
};

const fetchOrderStart = () => {
    return {
        type: actionTypes.FETCH_ORDER_START
    };
};

export const fetchOrders = (token, userId) => {
    return dispatch => {
        dispatch(fetchOrderStart());
        const queryParams =
            '?auth=' + token + '&orderBy="userId"&equalTo="' + userId + '"';
        axios
            .get('orders.json' + queryParams)
            .then(response => {
                const fetchedOrders = Object.keys(response.data).map(value => {
                    return {
                        ...response.data[value],
                        id: value
                    };
                });
                dispatch(fetchOrderSuccess(fetchedOrders));
            })
            .catch(err => {
                dispatch(fetchOrderFail(err));
            });
    };
};
