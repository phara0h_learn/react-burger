import * as actionTypes from './actionTypes';
import axios from '../../axios-orders';

export const addIngredient = ingredientName => {
    return { type: actionTypes.ADD_INGREDIENT, ingredientName };
};

export const removeIngredient = ingredientName => {
    return { type: actionTypes.REMOVE_INGREDIENT, ingredientName };
};

const setIngredients = ingredients => {
    return {
        type: actionTypes.SET_INGREDIENTS,
        ingredients
    };
};

const showError = error => {
    return {
        type: actionTypes.SHOW_ERROR,
        errorTxt: error
    };
};

export const initIngredients = () => {
    return dispatch => {
        axios
            .get('/ingredients.json')
            .then(res => {
                dispatch(setIngredients(res.data));
            })
            .catch(error => {
                dispatch(showError(error));
            });
    };
};
