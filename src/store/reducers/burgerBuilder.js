import { updateObject } from '../../shared/utility';
import * as actionTypes from '../actions/actionTypes';

const initialState = {
    ingredients: null,
    totalPrice: 4,
    error: false,
    building: false
};

const INGREDIENTS_PRICES = {
    salad: 0.75,
    cheese: 1,
    bacon: 1.75,
    meat: 4.75
};

const calculatePrice = ingredients => {
    const price = Object.keys(ingredients).map(value => {
        return ingredients[value] * INGREDIENTS_PRICES[value];
    });
    return price.reduce((a, b) => a + b, 0) + 4;
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_INGREDIENT:
            const updatedIngredientAdd = {
                [action.ingredientName]:
                    state.ingredients[action.ingredientName] + 1
            };
            const updatedIngredientsAdd = updateObject(
                state.ingredients,
                updatedIngredientAdd
            );
            const updatedStateAdd = {
                ingredients: updatedIngredientsAdd,
                totalPrice:
                    state.totalPrice + INGREDIENTS_PRICES[action.ingredientName],
                building: true
            };
            return updateObject(state, updatedStateAdd);
        case actionTypes.REMOVE_INGREDIENT:
            const updatedIngredientRem = {
                [action.ingredientName]:
                    state.ingredients[action.ingredientName] - 1
            };
            const updatedIngredientsRem = updateObject(
                state.ingredients,
                updatedIngredientRem
            );
            const updatedStateRem = {
                ingredients: updatedIngredientsRem,
                totalPrice:
                    state.totalPrice - INGREDIENTS_PRICES[action.ingredientName]
            };
            return updateObject(state, updatedStateRem);
        case actionTypes.SET_INGREDIENTS:
            return updateObject(state, {
                ingredients: {
                    salad: action.ingredients.salad,
                    bacon: action.ingredients.bacon,
                    cheese: action.ingredients.cheese,
                    meat: action.ingredients.meat
                },
                totalPrice: calculatePrice(action.ingredients),
                error: false,
                building: false
            });
        case actionTypes.SHOW_ERROR:
            return updateObject(state, {
                error: true
            });
    }

    return state;
};

export default reducer;
