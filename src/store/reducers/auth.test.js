import reducer, { initialState } from './auth';
import * as actionTypes from '../actions/actionTypes';

describe('Auth Reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState)
    })

    it('should store the token in the state', () =>{

        expect(reducer(initialState, {
            type: actionTypes.AUTH_SUCCESS,
            idToken: 'test_idToken',
            userId:  'test_userId'
        })).toEqual({
            ...initialState,
            token: 'test_idToken',
            userId:  'test_userId'
        })
    })
})