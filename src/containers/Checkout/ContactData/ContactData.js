import React, { Component } from 'react';
import { connect } from 'react-redux';

import axios from '../../../axios-orders';
import { updateObject, inputValidationCheck } from '../../../shared/utility';

import Spinner from '../../../components/UI/spinner/spinner';
import Button from '../../../components/UI/Button/Button';
import Input from '../../../components/UI/Input/Input';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import { purchaseBurger } from '../../../store/actions';

import classes from './ContactData.css';

class ContactData extends Component {
    state = {
        orderForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true,
                    valid: false
                },
                touched: false
            },
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Streetname'
                },
                value: '',
                validation: {
                    required: true,
                    valid: false
                },
                touched: false
            },
            town: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your City name'
                },
                value: '',
                validation: {
                    required: true,
                    valid: false
                },
                touched: false
            },
            zipcode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Zipcode'
                },
                value: '',
                validation: {
                    required: true,
                    maxLength: 5,
                    minLength: 5,
                    valid: false
                },
                touched: false
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Country'
                },
                value: '',
                validation: {
                    required: true,
                    valid: false
                },
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your E-Mail'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true,
                    valid: false
                },
                touched: false
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    config: {
                        placeholder: 'Select the Delivery method'
                    },
                    options: [
                        {
                            value: 'fastest',
                            displayValue: 'Fastest'
                        },
                        {
                            value: 'normal',
                            displayValue: 'Normal'
                        }
                    ]
                },
                value: 'NULL',

                touched: false
            }
        },
        formIsValid: false
    };

    submitDataHandler = event => {
        event.preventDefault();
        if (!this.state.formIsValid) {
            return;
        }
        const formData = {};
        for (let formElementId in this.state.orderForm) {
            formData[formElementId] = this.state.orderForm[formElementId].value;
        }

        const postData = {
            ingredients: this.props.ingredients,
            price: this.props.totalPrice,
            orderData: formData,
            userId: this.props.userId
        };

        this.props.purchaseBurger( postData, this.props.token );

    };

    inputValueChangeHandler = (event, elementId) => {


        const updatedFormElement = updateObject(this.state.orderForm[elementId], {
            value: event.target.value,
            touched: true,
        })

        const updatedFormElementValidation = updateObject( updatedFormElement.validation, {
            valid: inputValidationCheck(
                updatedFormElement.value,
                updatedFormElement.validation
            )
        })
        updatedFormElement.validation = updatedFormElementValidation;

        const updatedOderForm = updateObject(this.state.orderForm, {
            [elementId]: updatedFormElement
        });

        let formIsValid = true;

        for (const key in updatedOderForm) {
            if (updatedOderForm.hasOwnProperty(key)) {
                if (updatedOderForm[key].validation) {
                    formIsValid =
                        updatedOderForm[key].validation.valid && formIsValid;
                }
            }
        }

        this.setState({
            orderForm: updatedOderForm,
            formIsValid: formIsValid
        });
    };

    render() {
        const orderForm = { ...this.state.orderForm };
        const formElement = Object.keys(orderForm).map(inName => {
            return (
                <Input
                    key={inName}
                    elementType={orderForm[inName].elementType}
                    elementConfig={orderForm[inName].elementConfig}
                    value={orderForm[inName].value}
                    isInvalid={
                        orderForm[inName].validation
                            ? !orderForm[inName].validation.valid
                            : false
                    }
                    shouldVaidate={orderForm[inName].validation ? true : false}
                    isTouched={orderForm[inName].touched}
                    changeValue={event =>
                        this.inputValueChangeHandler(event, inName)
                    }
                />
            );
        });
        let form = (
            <form onSubmit={this.submitDataHandler}>
                {formElement}
                <Button btnType="Success" disabled={!this.state.formIsValid}>
                    Order
                </Button>
            </form>
        );

        if (this.props.loading) {
            form = <Spinner />;
        }
        return (
            <div className={classes.ContactData}>
                <h4>Please enter your details</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burger.ingredients,
        totalPrice: state.burger.totalPrice,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        purchaseBurger: (orderData, token) => dispatch( purchaseBurger( orderData, token ) )
    };
};


export default connect(mapStateToProps, mapDispatchToProps)( withErrorHandler(ContactData, axios) );
