import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import CheckoutSummery from '../../components/Order/CheckoutSummery/CheckoutSummery';
import ContactData from './ContactData/ContactData';

class Checkout extends Component {

    checkoutCancelHandler = () => {
        this.props.history.push({
            pathname: '/',
            search : this.props.location.search
        });
    }

    checkoutContinueHandler = () => {
        this.props.history.replace({
            pathname : this.props.match.path + '/contact-data',
            search :this.props.location.search
        });
    }

    render() {
        if ( this.props.ingredients === null ) {
            return <Redirect to="/" />
        }
        const purchasedRedirect = this.props.purchased ? <Redirect to="/" /> : null
        return (

            <div>
                {purchasedRedirect}
                <CheckoutSummery
                    checkoutCanceld={this.checkoutCancelHandler}
                    checkoutContinue={this.checkoutContinueHandler}
                    ingredients={this.props.ingredients} />
                <Route path={this.props.match.path + '/contact-data'} component={ContactData} />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burger.ingredients,
        totalPrice: state.burger.totalPrice,
        purchased: state.order.purchased
    };
};


export default connect( mapStateToProps )(Checkout);