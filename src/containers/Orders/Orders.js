import React, { Component } from 'react';
import { connect } from 'react-redux';

import errorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axios-orders';

import classes from './Orders.css';

import Order from '../../components/Order/Order';
import Loading from '../../components/UI/spinner/spinner';
import { fetchOrders } from '../../store/actions';

class Orders extends Component {
    state = {
        orders : [],
        loading : true
    }

    componentDidMount () {
        this.props.onFetchOrders(this.props.token, this.props.userId);
    }

    render() {
        let Orders = <Loading />;
        if (!this.props.loading){
            Orders = this.props.orders.map(value => {
                return <Order key={value.id} {...value} />
            })
        }
        return (
            <div className={classes.Orders}>
                <h1>Orders</h1>
                {Orders}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        orders: state.order.orders,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchOrders : (token, userId) => dispatch( fetchOrders(token, userId) )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)( errorHandler(Orders, axios) );