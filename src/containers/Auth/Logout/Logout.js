import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { logout } from '../../../store/actions';
import Spinner from '../../../components/UI/spinner/spinner';

class Logout extends Component {

    componentDidMount(){
        if (this.props.isAuthenticated) {
            this.props.onLogout();
        }
    }

    render() {

        return ( this.props.isAuthenticated ? <Spinner/> : <Redirect to="/" /> )
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogout : () => dispatch( logout() )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout);