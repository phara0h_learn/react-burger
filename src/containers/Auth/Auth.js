import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/spinner/spinner';
import classes from './Auth.css';
import { auth, setAuthRedirectPath } from '../../store/actions';
import { updateObject, inputValidationCheck } from '../../shared/utility';

class Auth extends Component {
    state = {
        isSignUp: true,
        form: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Login E-Mail'
                },
                value: '',
                validation: {
                    required: true,
                    valid: false,
                    isEmail: true
                },
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    valid: false,
                    minLenght: 8
                },
                touched: false
            }
        }
    };

    componentDidMount() {
        if ( !this.props.isBuilding && this.props.authRedirectPath !== '/' ) {
            this.props.onSetAuthRedirectPath('/');
        }
    }

    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return {
                isSignUp: !prevState.isSignUp
            };
        });
    };

    inputChangeHandler = (event, formName) => {
        const updatedForm = updateObject( this.state.form, {
            [formName]:
                updateObject( this.state.form[formName], {
                value: event.target.value,
                validation: updateObject( this.state.form[formName].validation, {
                    valid: inputValidationCheck(
                        event.target.value,
                        this.state.form[formName].validation
                    )
                }),
                touched: true
            })
        });

        this.setState({ form: updatedForm });
    };

    submitHandler = event => {
        event.preventDefault();
        const method = this.state.isSignUp ? 'signup' : 'login';
        this.props.onAuth(
            this.state.form.email.value,
            this.state.form.password.value,
            method
        );
    };

    render() {
        let formElement = Object.keys(this.state.form).map(inName => {
            return (
                <Input
                    key={inName}
                    elementType={this.state.form[inName].elementType}
                    elementConfig={this.state.form[inName].elementConfig}
                    value={this.state.form[inName].value}
                    isInvalid={
                        this.state.form[inName].validation
                            ? !this.state.form[inName].validation.valid
                            : false
                    }
                    shouldVaidate={
                        this.state.form[inName].validation ? true : false
                    }
                    isTouched={this.state.form[inName].touched}
                    changeValue={event =>
                        this.inputChangeHandler(event, inName)
                    }
                />
            );
        });

        if ( this.props.loading ) {
            formElement = <Spinner />;
        }

        const errorMessage = this.props.error ? <div className={classes.errorMessage}>{this.props.error.message}</div> : null

        return (
            <div className={classes.Auth}>
                {errorMessage}
                <form onSubmit={this.submitHandler}>
                    {formElement}
                    <Button btnType="Success">
                        {this.state.isSignUp ? 'Sign up' : 'Login'}
                    </Button>
                </form>
                <Button clicked={this.switchAuthModeHandler} btnType="Danger">
                    {this.state.isSignUp ? 'Login' : 'Sign up'} instead
                </Button>
                {!this.props.isAuthenticated ? null :  <Redirect to={this.props.redirectPath} />}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        redirectPath: state.auth.authRedirectPath,
        isBuilding: state.burger.building
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password, method) =>
            dispatch(auth(email, password, method)),
        onSetAuthRedirectPath: (path) => dispatch( setAuthRedirectPath(path) )
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Auth);
