import React, { Component } from 'react';
import { connect } from 'react-redux';

import axios from '../../axios-orders';

import Aux from '../../hoc/_Aux/_Aux';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControles/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummery from '../../components/Burger/OrderSummery/OrderSummery';
import Spinner from '../../components/UI/spinner/spinner';
import { addIngredient, removeIngredient, initIngredients, initPurchaseBurger, setAuthRedirectPath } from '../../store/actions';


export class BurgerBuilder extends Component {
    state = {
        purchasing: false
    };

    componentDidMount() {
        this.props.initIngredient()
        // if (this.props.ingredients === null ) {
        // }
    }

    updatePurchasableState = ingredients => {
        const sum = Object.keys(ingredients)
            .map(igKey => {
                return ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);

        return sum > 0
    };

    purchaseHandler = () => {
        if ( this.props.isAuthenticated) {
            this.setState({ purchasing: true });
        } else {
            this.props.setAuthRedirectPath('/checkout')
            this.props.history.push({
                pathname: '/auth'
            });
        }
    };

    purchaseCloseHandler = () => {
        this.setState({ purchasing: false });
    };

    purchaseContinueHandler = () => {
        this.props.initPurchaseBurger();
        this.props.history.push({
            pathname: '/checkout'
        });
    };

    render() {
        let orderSummery = null;

        let burger = this.props.error ? (
            <p style={{ textAlign: 'center' }}>
                The ingredients could not be loaded!
            </p>
        ) : (
            <Spinner />
        );

        if (this.props.ingredients) {
            const disabledCntrl = {
                ...this.props.ingredients
            };

            for (let key in disabledCntrl) {
                disabledCntrl[key] = disabledCntrl[key] <= 0 ? true : false;
            }

            orderSummery = (
                <OrderSummery
                    clickToClose={this.purchaseCloseHandler}
                    clickToPurchase={this.purchaseContinueHandler}
                    ingredients={this.props.ingredients}
                    price={this.props.totalPrice}
                />
            );

            burger = (
                <Aux>
                    <Burger ingredients={this.props.ingredients} />
                    <BuildControls
                        clickMore={this.props.addIngredient}
                        clickLess={this.props.removeIngredient}
                        disabledCntrl={disabledCntrl}
                        price={this.props.totalPrice}
                        purchasable={this.updatePurchasableState(this.props.ingredients)}
                        orderbtn={this.purchaseHandler}
                        isAuth={this.props.isAuthenticated}
                    />
                </Aux>
            );
        }

        return (
            <Aux>
                <Modal
                    show={this.state.purchasing}
                    clickToClose={this.purchaseCloseHandler}
                >
                    {orderSummery}
                </Modal>
                {burger}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        ingredients: state.burger.ingredients,
        totalPrice: state.burger.totalPrice,
        error: state.burger.error,
        isAuthenticated: state.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addIngredient: (ingredientName) => dispatch( addIngredient(ingredientName) ),
        removeIngredient: (ingredientName) => dispatch( removeIngredient(ingredientName) ),
        initIngredient: () => dispatch( initIngredients() ),
        initPurchaseBurger: () => dispatch( initPurchaseBurger() ),
        setAuthRedirectPath: (path) => dispatch( setAuthRedirectPath(path) )
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withErrorHandler(BurgerBuilder, axios));
