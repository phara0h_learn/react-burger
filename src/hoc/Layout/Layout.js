import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from '../_Aux/_Aux';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDraw from '../../components/Navigation/SideDraw/SideDraw';

import classes from './Layout.css';


class Layout extends Component {
    state = {
        showSidedraw : false
    }

    sidedrawToggleHandler = () => {
        this.setState( (prevState) =>{
            return { showSidedraw: !prevState.showSidedraw }
        });
    }

    sidedrawClosedHandler = () => {
        this.setState({ showSidedraw:false });
    }

    render () {
        return (
            <Aux>
                <Toolbar
                    isAuth={this.props.isAuthenticated}
                    sidedrawToggle={this.sidedrawToggleHandler} />
                <SideDraw
                    isAuth={this.props.isAuthenticated}
                    showSD={this.state.showSidedraw}
                    closeSD={this.sidedrawClosedHandler} />
                <div> Sidedraw, Backdrop</div>
                <main className={classes.Main}>
                    {this.props.children}
                </main>
            </Aux>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        isAuthenticated: state.auth.token !== null
    }
}

export default connect(mapStateToProps)(Layout);