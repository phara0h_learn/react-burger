import React, { Component } from 'react';

import Aux from '../_Aux/_Aux';
import Modal from '../../components/UI/Modal/Modal';

const withErrorHandler = ( WrappedComponent, axios ) => {
    return class extends Component {
        state = {
            error : null
        }

        componentWillMount() {
            this.reqInterceptor = axios.interceptors.request.use(req => {
                this.setState({ error : null })
                return req;
            }, error => {
                this.setState({ error : error })
                //return error;
            })

            this.resInterceptor = axios.interceptors.response.use(res => res, error => {
                this.setState({ error : error })
                //return error;
            })
        }

        componentWillUnmount() {
            axios.interceptors.request.eject( this.reqInterceptor )
            axios.interceptors.response.eject( this.resInterceptor )
        }

        errorCloseHandler = () => {
            this.setState({error:null});
        }

        render() {
            return (
                <Aux>
                    <Modal
                        show={this.state.error}
                        clickToClose={this.errorCloseHandler} >
                        <div>Something whent wrong with the request.</div>
                        <div>{this.state.error ? this.state.error.message : null}</div>
                    </Modal>
                    <WrappedComponent {...this.props} />
                </Aux>
            )
        }
    }
}


export default withErrorHandler;