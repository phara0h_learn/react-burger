import axios from 'axios';

const instance = axios.create({
    baseURL : 'https://burger-react-1dd1c.firebaseio.com/'
})

export default instance;