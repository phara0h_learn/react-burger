import React from 'react';

import classes from './Input.css';

const input = props => {
    let inputElement = null;
    const inputClasses = [classes.inputElement];

    if ( props.isInvalid && props.shouldVaidate && props.isTouched ) {
        inputClasses.push(classes.invalid)
    }

    switch (props.elementType) {
        case 'textArea':
            inputElement = (
                <textarea
                    {...props.elementConfig}
                    className={inputClasses.join(' ')}
                    value={props.value}
                    onChange={props.changeValue}
                />
            );

            break;

        case 'select':
            inputElement = (
                <select
                    className={inputClasses.join(' ')}
                    {...props.elementConfig.config}
                    value={props.value}
                    onChange={props.changeValue}
                >
                    <option disabled value="NULL">
                        {props.elementConfig.config.placeholder}
                    </option>
                    {props.elementConfig.options.map(option => {
                        return (
                            <option key={option.value} value={option.value}>
                                {option.displayValue}
                            </option>
                        );
                    })}
                </select>
            );
            break;

        default:
            inputElement = (
                <input
                    className={inputClasses.join(' ')}
                    {...props.elementConfig}
                    value={props.value}
                    onChange={props.changeValue}
                />
            );
            break;
    }

    return (
        <div className={classes.inputDiv}>
            <label className={classes.label}>{props.label}</label>
            {inputElement}
        </div>
    );
};

export default input;
