import React from 'react';

import classes from './Order.css';

const order = (props) =>  (
        <div className={classes.Order}>
            <h3>Ingredients</h3>
            <ul>
                {Object.keys(props.ingredients).map( value => {
                    return props.ingredients[value] !== 0 ? <li key={props.id + value}>{value} ({props.ingredients[value]})</li> : null
                })}

            </ul>
            <p>Price <strong>USD {props.price}</strong></p>
        </div>
)


export default order;