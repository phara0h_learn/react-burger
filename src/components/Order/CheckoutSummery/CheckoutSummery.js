import React from 'react';

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';

import classes from './CheckoutSummery.css';

const checkoutSummery = (props) => {
    return (
        <div className={classes.CheckoutSummery}>
            <h1>Hope you like the taste of the burger!</h1>
            <div className={classes.Burger} >
                <Burger ingredients={props.ingredients} />
            </div>
            <Button
                clicked={props.checkoutCanceld}
                btnType="Danger">Cancel</Button>
            <Button
                clicked={props.checkoutContinue}
                btnType="Success">Continue</Button>
        </div>
    )
}

export default checkoutSummery;