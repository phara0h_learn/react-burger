import React from 'react';

import BurgerIngredients from './BurgerIngredients/BurgerIngredients';

import classes from './Burger.css';

const burger = (props) => {
    let transfomedIngredients = Object.keys(props.ingredients)
        .map(igKey => {
            return [...Array(props.ingredients[igKey])].map((_, i) => {
                return <BurgerIngredients key={igKey + i} type={igKey} />;
            })
        })
        .reduce( (arr, el) => {
            return arr.concat(el)
        },[])
    if ( transfomedIngredients.length === 0 ) {
        transfomedIngredients = <p>Please add some ingredients</p>;
    }

    return <div className={classes.Burger}>
            <BurgerIngredients type="bread-top" />
            {transfomedIngredients}
            <BurgerIngredients type="bread-bottom" />
        </div>;
};

export default burger;