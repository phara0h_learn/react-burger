import React from 'react';

import BuildControl from './Buildcontol/BuildControl';

import classes from './BuildControls.css';

const controles = [
    {label: 'Salad', type : 'salad'},
    {label: 'Bacon', type : 'bacon'},
    {label: 'Cheese', type : 'cheese'},
    {label: 'Meat', type : 'meat'}
];

const buildControls = (props) => (
    <div className={classes.BuildControls}>
        <p>Current Price: <strong>{props.price.toFixed(2)}</strong></p>
        {controles.map(control => {
            return <BuildControl
                key={control.label}
                label={control.label}
                type={control.type}
                clickMore={() => props.clickMore( control.type )}
                clickLess={() => props.clickLess( control.type )}
                isDisabled={ props.disabledCntrl[control.type] }
            />
        })}
        <button
            disabled={!props.purchasable}
            className={classes.OrderButton}
            onClick={props.orderbtn}>{props.isAuth ? 'Order NOW' : 'Sign-up & Order'}</button>
    </div>
);


export default buildControls;