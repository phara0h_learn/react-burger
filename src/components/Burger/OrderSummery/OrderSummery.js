import React from 'react';
import Aux from '../../../hoc/_Aux/_Aux';

import Button from '../../UI/Button/Button';

const orderSummery = (props) => {
    const ingredientSummery = Object.keys( props.ingredients )
        .map( ingrKey => {
            return <li key={ingrKey}>
                <span style={{textTransform: 'capitalize'}}>{ingrKey}</span>: {props.ingredients[ingrKey]}
            </li>
        } );
    return <Aux>
        <h3>Your Order</h3>
        <p>A delicious burger with the following ingredients:</p>
        <ul style={{padding: 'inherit'}}>
            {ingredientSummery}
        </ul>
        <p><strong>Total Price: {props.price.toFixed(2)}</strong></p>
        <p>Continue to Checkout?</p>
        <Button
            btnType="Danger"
            clicked={props.clickToClose} >CANCEL</Button>
        <Button
            btnType="Success"
            clicked={props.clickToPurchase} >CONTINUE</Button>
    </Aux>
}

export default orderSummery;