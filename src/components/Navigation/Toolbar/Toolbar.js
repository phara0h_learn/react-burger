import React from 'react';

import classes from './Toolbar.css';

import Logo from '../../Logo/Logo';
import NavigationList from '../NavigationItems/NavigationItems';
import MenuBtn from '../SideDraw/MenuBtn/MenuBtn';

const toolbar = (props) => (
    <header className={classes.Toolbar}>
        <MenuBtn clicked={props.sidedrawToggle}/>
        <div className={classes.Logo}>
            <Logo />
        </div>
        <nav className={classes.DesktopOnly}>
            <NavigationList isAuthenticated={props.isAuth} />
        </nav>
    </header>
)

export default toolbar;