import React from 'react';

import classes from './MenuBtn.css';

const menuBtn = (props) => (
    <div className={classes.MenuBtn} onClick={props.clicked}>
        <div></div>
        <div></div>
        <div></div>
    </div>
)

export default menuBtn;