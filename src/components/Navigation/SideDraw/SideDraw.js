import React from 'react';

import Logo from '../../Logo/Logo';
import NavigationList from '../NavigationItems/NavigationItems';
import Backdrop from '../../UI/Backdrop/Backdrop';
import Aux from '../../../hoc/_Aux/_Aux';

import classes from './SideDraw.css';

const sideDraw = (props) => {
    let classesSD = [classes.SideDraw, classes.Close];
    if (props.showSD) {
        classesSD = [classes.SideDraw, classes.Open];
    }
    return (
        <Aux>
            <Backdrop
                show={props.showSD}
                clicked={props.closeSD}
            />
            <div className={classesSD.join(' ')} onClick={props.closeSD}>
                <div className={classes.Logo}>
                    <Logo />
                </div>
                <nav>
                    <NavigationList isAuthenticated={props.isAuth} />
                </nav>
            </div>
        </Aux>
    )
}

export default sideDraw;